package utilities

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
)

// SaveJSONByteSliceToPath ...
func SaveJSONByteSliceToPath(model interface{}, directory string, fileName string) error {
	var byteSlice []byte
	var err error
	var file *os.File
	var path string
	path = filepath.Join(directory, RemoveInvalidWindowsPathCharacters(fileName))
	byteSlice, err = json.MarshalIndent(model, "", "    ")
	if err == nil {
		fmt.Println(path)
		file, err = os.Create(path)
		if err == nil {
			defer file.Close()
			_, err = file.Write(byteSlice)
			if err == nil {
				_, err = file.Write([]byte("\n"))
				err = file.Sync()
			}
		}
	}
	return err
}
