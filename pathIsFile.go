package utilities

import "os"

// PathIsFile ...
func PathIsFile(path string) bool {
	var result = false
	var err error
	var fileInfo os.FileInfo
	fileInfo, err = os.Stat(path)
	if err == nil {
		result = fileInfo.Mode().IsRegular()
	}
	return result
}
