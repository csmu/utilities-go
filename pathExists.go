package utilities

import "os"

// PathExists ...
func PathExists(path string) bool {
	var result = false
	var err error
	_, err = os.Stat(path)
	if err == nil {
		result = true
	}
	return result
}
