package utilities

import "os"

// PathIsDirectory ...
// https://stackoverflow.com/a/49697453/162358
func PathIsDirectory(path string) bool {
	var result = false
	var err error
	var fileInfo os.FileInfo
	fileInfo, err = os.Stat(path)
	if err == nil {
		result = fileInfo.Mode().IsDir()
	}
	return result
}
