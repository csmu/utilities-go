package utilities

import (
	"os"
	"strings"
)

// GetEnvironmentVariable ...
func GetEnvironmentVariable(key string) string {

	var value string
	var keyValuePair string
	var pair []string

	for _, keyValuePair = range os.Environ() {
		pair = strings.Split(keyValuePair, "=")
		switch pair[0] {
		case key:
			value = pair[1]
		}
	}

	return value
}
